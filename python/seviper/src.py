import json

DATA_PATH = "../go/bippa/data/"
POKEDEX_PATH = DATA_PATH + "pokedex.json"
MOVEDEX_PATH = DATA_PATH + "movedex.json"
NATUREDEX_PATH = DATA_PATH + "naturedex.json"

def load_pokedex():
    with open(POKEDEX_PATH, "r", encoding="utf-8") as file_obj:
        return json.load(file_obj)

def load_movedex():
    with open(MOVEDEX_PATH, "r", encoding="utf-8") as file_obj:
        return json.load(file_obj)

def load_naturedex():
    with open(NATUREDEX_PATH, "r", encoding="utf-8") as file_obj:
        return json.load(file_obj)

POKEDEX = load_pokedex()
MOVEDEX = load_movedex()
NATUREDEX = load_naturedex()

def id_to_key(dex, dex_id):
    results = []
    for key in dex:
        if dex_id == dex[key]["ID"]:
            results.append(key)
    assert len(results) == 1
    return results[0]

def get_data_base_poke_names():
    return [id_to_key(POKEDEX, i) for i in range(len(POKEDEX))]

def get_data_base_move_names():
    return [id_to_key(MOVEDEX, i) for i in range(len(MOVEDEX))]

def get_data_base_nature_names():
    result = [id_to_key(NATUREDEX, i) for i in range(len(NATUREDEX))]
    assert len(result) == 25
    return result

DATA_BASE_POKE_NAMES = get_data_base_poke_names()
DATA_BASE_MOVE_NAMES = get_data_base_move_names()
DATA_BASE_NATURE_NAMES = get_data_base_nature_names()

if __name__ == "__main__":
    print(POKEDEX)
    print(MOVEDEX)
    print(NATUREDEX)
    print(DATA_BASE_POKE_NAMES)
    print(DATA_BASE_MOVE_NAMES)
    print(DATA_BASE_NATURE_NAMES)
