import socket
import tensorflow as tf
import nana.src as nana

def send_team_info(conn, team_info, color):
    for key, value in team_info.items():
        msg_list = [str(ele) if ele is not None else "None" for ele in value]
        msg = (key + " ") + " ".join(msg_list)
        assert color == conn.recv(1024).decode("utf-8")
        conn.sendall(msg.encode("utf-8"))

def main():
    session = tf.Session()
    network = nana.Network(session)
    session.run(tf.global_variables_initializer())

    red_input_data, red_action_data = nana.team_build(1, network)
    blue_input_data, blue_action_data = nana.team_build(1, network)

    red_team_info = nana.input_data_to_team_info(red_input_data)
    blue_team_info = nana.input_data_to_team_info(blue_input_data)

    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.bind(("127.0.0.1", 7777))
    server.listen(1)

    print("クライアントの接続待ち...")
    conn, addr = server.accept()
    print("クライアントの接続完了")

    send_team_info(conn, red_team_info, "red")
    send_team_info(conn, blue_team_info, "blue")

if __name__ == "__main__":
    main()
