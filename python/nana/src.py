import random
import copy
from collections import OrderedDict
import numpy as np
import tensorflow as tf
import crow.src as crow
import seviper.src as seviper

class Network:
    """
    ポケモンの名前 0～5層
    レベル 6～11層
    性格 12～17層

    技1 18～23層
    技2 24～29層
    技3 30～35層
    技4 36～41層

    HP個体値 42～47層
    攻撃個体値 48～53層
    防御個体値 54～59層
    特攻個体値 60～65層
    特防個体値 66～71層
    素早さ個体値 72～77層

    HP努力値 78～83層
    攻撃努力値 84～89層
    防御努力値 90～95層
    特攻努力値 96～101層
    特防努力値 102～107層
    素早さ努力値 108～113層
    """

    #特徴量のパターンが最も多いのは、努力値の252パターン
    #252パターンの特徴量を二次元で表現しようとした場合、16 * 16 = 256が最もコンパクトに収まるサイズである
    INPUT_SHAPE = 16, 16, 114
    HEIGTH = INPUT_SHAPE[0]
    WIDTH = INPUT_SHAPE[1]
    DEPTH = INPUT_SHAPE[2]
    OUTPUT_SIZE = 1

    def __init__(self, session):
        self.session = session

        self.input_holder = tf.placeholder(tf.float32, [None] + list(Network.INPUT_SHAPE))
        self.label_holder = tf.placeholder(tf.float32, [None] + [Network.OUTPUT_SIZE])
        self.is_training_holder = tf.placeholder(tf.bool)
        self.keep_holder = tf.placeholder(tf.float32)

        stddev = 0.01

        filter1_size = 128
        filter1 = crow.random_variable([3, 3, Network.INPUT_SHAPE[-1], filter1_size], stddev)
        conv_alpha1 = crow.zeros_variable([filter1_size])

        filter2_size = 128
        filter2 = crow.random_variable([3, 3, filter1_size, filter2_size], stddev)
        conv_alpha2 = crow.zeros_variable([filter2_size])

        filter3_size = 128
        filter3 = crow.random_variable([3, 3, filter2_size, filter3_size], stddev)
        conv_alpha3 = crow.zeros_variable([filter3_size])

        filter4_size = 128
        filter4 = crow.random_variable([3, 3, filter3_size, filter4_size], stddev)
        conv_alpha4 = crow.zeros_variable([filter4_size])

        filter5_size = 128
        filter5 = crow.random_variable([3, 3, filter4_size, filter5_size], stddev)
        conv_alpha5 = crow.zeros_variable([filter5_size])

        flat_size = Network.INPUT_SHAPE[0] * Network.INPUT_SHAPE[1] * filter5_size

        neuron1_size = 128
        w1 = crow.random_variable([flat_size, neuron1_size], stddev)
        affine_alpha1 = crow.zeros_variable([neuron1_size])

        w2 = crow.random_variable([neuron1_size, Network.OUTPUT_SIZE], stddev)
        b2 = crow.random_variable([Network.OUTPUT_SIZE], stddev)
        affine_alpha2 = crow.zeros_variable([Network.OUTPUT_SIZE])

        conv_layer1 = crow.conv_BN_prelu_dropout(self.input_holder, filter1, self.is_training_holder,
                                                 conv_alpha1, self.keep_holder)
        conv_layer2 = crow.conv_BN_prelu_dropout(conv_layer1, filter2, self.is_training_holder,
                                                 conv_alpha2, self.keep_holder)
        conv_layer3 = crow.conv_BN_prelu_dropout(conv_layer2, filter3, self.is_training_holder,
                                                 conv_alpha3, self.keep_holder)
        conv_layer4 = crow.conv_BN_prelu_dropout(conv_layer3, filter4, self.is_training_holder,
                                                 conv_alpha4, self.keep_holder)
        conv_layer5 = crow.conv_BN_prelu_dropout(conv_layer4, filter5, self.is_training_holder,
                                                 conv_alpha5, self.keep_holder)
        flat = crow.flatten(conv_layer5)

        affine_layer1 = crow.matmul_BN_prelu_dropout(flat, w1, self.is_training_holder, affine_alpha1, self.keep_holder)
        last_matmul = tf.matmul(affine_layer1, w2) + b2
        last_prelu = crow.prelu(last_matmul, affine_alpha2)
        self.output = tf.nn.softmax(last_prelu)

        self.loss = crow.cross_entropy(self.output, self.label_holder)

        #12 = BatchNormalizationの数 * 2
        #インスタンス化で複数のネットワークを生成した時に、この一文がなければエラーが起きる
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)[-12:]
        assert len(update_ops) == 12

        with tf.control_dependencies(update_ops):
            self.optimizer = crow.AdaBoundOptimizer().minimize(self.loss)
        self.saver = tf.train.Saver()

    def run_output(self, input_data):
        return self.session.run(self.output, feed_dict={self.input_holder:input_data, self.is_training_holder:False,
                                                        self.keep_holder:1.0})

    def run_loss(self, input_data, label_data):
        return self.session.run(self.loss, feed_dict={self.input_holder:input_data, self.is_training_holder:False,
                                                      self.keep_holder:1.0})

    def run_optimizer(self, input_data, label_data, keep):
        self.session.run(self.optimizer, feed_dict={self.input_holder:input_data, self.is_training_holder:True,
                                                    self.keep_holder:keep})

count_gene = (i for i in range(Network.DEPTH))

POKE_DEPTHS = [next(count_gene) for i in range(6)]
LEVEL_DEPTHS = [next(count_gene) for i in range(6)]
NATURE_DEPTHS = [next(count_gene) for i in range(6)]
MOVE1_DEPTHS = [next(count_gene) for i in range(6)]
MOVE2_DEPTHS = [next(count_gene) for i in range(6)]
MOVE3_DEPTHS = [next(count_gene) for i in range(6)]
MOVE4_DEPTHS = [next(count_gene) for i in range(6)]
HP_IV_DEPTHS = [next(count_gene) for i in range(6)]
ATK_IV_DEPTHS = [next(count_gene) for i in range(6)]
DEF_IV_DEPTHS = [next(count_gene) for i in range(6)]
SP_ATK_IV_DEPTHS = [next(count_gene) for i in range(6)]
SP_DEF_IV_DEPTHS = [next(count_gene) for i in range(6)]
SPEED_IV_DEPTHS = [next(count_gene) for i in range(6)]
HP_EV_DEPTHS = [next(count_gene) for i in range(6)]
ATK_EV_DEPTHS = [next(count_gene) for i in range(6)]
DEF_EV_DEPTHS = [next(count_gene) for i in range(6)]
SP_ATK_EV_DEPTHS = [next(count_gene) for i in range(6)]
SP_DEF_EV_DEPTHS = [next(count_gene) for i in range(6)]
SPEED_EV_DEPTHS = [next(count_gene) for i in range(6)]

DATA_BASE_POKE_NAMES = ["なし"] + seviper.DATA_BASE_POKE_NAMES
DATA_BASE_LEVELS = [i + 1 for i in range(50)]
DATA_BASE_MOVE_NAMES = ["なし"] + seviper.DATA_BASE_MOVE_NAMES
DATA_BASE_IVS = [i for i in range(32)]
DATA_BASE_EVS = [i for i in range(253)]

POKE_FEATURE_NUM = len(DATA_BASE_POKE_NAMES)
LEVEL_FEATURE_NUM = len(DATA_BASE_LEVELS)
NATURE_FEATURE_NUM = len(seviper.DATA_BASE_NATURE_NAMES)
MOVE_FEATURE_NUM = len(DATA_BASE_MOVE_NAMES)
IV_FEATURE_NUM = len(DATA_BASE_IVS)
EV_FEATURE_NUM = len(DATA_BASE_EVS)

def get_init_input_data():
    return np.zeros(Network.INPUT_SHAPE)

def get_all_2d_coords(feature_num):
    result = []
    count = 0

    for h in range(Network.HEIGTH):
        for w in range(Network.WIDTH):
            result.append((h, w))
            count += 1
            if feature_num == count:
                return result
    return result

def input_data_to_feature(input_data, frame_num, depths, feature_num, data_base_features):
    depth = depths[frame_num]
    all_2d_coords = get_all_2d_coords(feature_num)
    for i, coord in enumerate(all_2d_coords):
        h, w = coord
        if input_data[h][w][depth] != 0.0:
            return data_base_features[i]
    return None

def input_data_to_poke_name(input_data, frame_num):
    return input_data_to_feature(input_data, frame_num, POKE_DEPTHS, POKE_FEATURE_NUM,
                                 DATA_BASE_POKE_NAMES)

def input_data_to_poke_names(input_data):
    return [input_data_to_poke_name(input_data, i) for i in range(6)]

def input_data_to_level(input_data, frame_num):
    return input_data_to_feature(input_data, frame_num, LEVEL_DEPTHS, LEVEL_FEATURE_NUM,
                                 DATA_BASE_LEVELS)

def input_data_to_levels(input_data):
    return [input_data_to_level(input_data, frame_num) for frame_num in range(6)]

def input_data_to_nature_name(input_data, frame_num):
    return input_data_to_feature(input_data, frame_num, NATURE_DEPTHS, NATURE_FEATURE_NUM,
                                 seviper.DATA_BASE_NATURE_NAMES)

def input_data_to_nature_names(input_data):
    return [input_data_to_nature_name(input_data, frame_num) for frame_num in range(6)]

def input_data_to_move(input_data, frame_num, depths):
    return input_data_to_feature(input_data, frame_num, depths, MOVE_FEATURE_NUM,
                                 DATA_BASE_MOVE_NAMES)

def input_data_to_move_names(input_data, frame_num):
    move1 = input_data_to_move(input_data, frame_num, MOVE1_DEPTHS)
    move2 = input_data_to_move(input_data, frame_num, MOVE2_DEPTHS)
    move3 = input_data_to_move(input_data, frame_num, MOVE3_DEPTHS)
    move4 = input_data_to_move(input_data, frame_num, MOVE4_DEPTHS)
    return [move1, move2, move3, move4]

def input_data_to_iv(input_data, frame_num, depths):
    return input_data_to_feature(input_data, frame_num, depths, IV_FEATURE_NUM, DATA_BASE_IVS)

def input_data_to_iv_stats(input_data, frame_num):
    hp = input_data_to_iv(input_data, frame_num, HP_IV_DEPTHS)
    atk = input_data_to_iv(input_data, frame_num, ATK_IV_DEPTHS)
    defe = input_data_to_iv(input_data, frame_num, DEF_IV_DEPTHS)
    sp_atk = input_data_to_iv(input_data, frame_num, SP_ATK_IV_DEPTHS)
    sp_def = input_data_to_iv(input_data, frame_num, SP_DEF_IV_DEPTHS)
    speed = input_data_to_iv(input_data, frame_num, SPEED_IV_DEPTHS)
    return [hp, atk, defe, sp_atk, sp_def, speed]

def input_data_to_ev(input_data, frame_num, depths):
    return input_data_to_feature(input_data, frame_num, depths, EV_FEATURE_NUM, DATA_BASE_EVS)

def input_data_to_ev_stats(input_data, frame_num):
    hp = input_data_to_ev(input_data, frame_num, HP_EV_DEPTHS)
    atk = input_data_to_ev(input_data, frame_num, ATK_EV_DEPTHS)
    defe = input_data_to_ev(input_data, frame_num, DEF_EV_DEPTHS)
    sp_atk = input_data_to_ev(input_data, frame_num, SP_ATK_EV_DEPTHS)
    sp_def = input_data_to_ev(input_data, frame_num, SP_DEF_EV_DEPTHS)
    speed = input_data_to_ev(input_data, frame_num, SPEED_EV_DEPTHS)
    return [hp, atk, defe, sp_atk, sp_def, speed]

def get_coord_evaluation_values(input_data, frame_num, network, depths, size):
    depth = depths[frame_num]
    batch_input_data = [copy.deepcopy(input_data) for _ in range(size)]
    all_2d_coords = get_all_2d_coords(size)

    for i, coord in enumerate(all_2d_coords):
        h, w = coord
        assert batch_input_data[i][h][w][depth] == 0.0
        batch_input_data[i][h][w][depth] = 1.0

    evaluation_values = [value[0] for value in network.run_output(batch_input_data)]
    result = OrderedDict()

    for i, coord in enumerate(all_2d_coords):
        h, w = coord
        result[(h, w, depth)] = evaluation_values[i]
    return result

def get_pokemon_coord_evaluation_values(input_data, frame_num, network):
    poke_names = input_data_to_poke_names(input_data)
    assert poke_names[frame_num] is None
    assert None not in poke_names[:frame_num]

    result = get_coord_evaluation_values(input_data, frame_num, network, POKE_DEPTHS, POKE_FEATURE_NUM)

    for i, key in enumerate(result):
        if i == 0:
            continue

        if DATA_BASE_POKE_NAMES[i] in poke_names:
            result[key] = -999

    depth = POKE_DEPTHS[frame_num]
    if frame_num <= 2:
        result[(0, 0, depth)] = -999

    assert not all([value == -999 for value in result.values()])
    return result

def get_level_coord_evaluation_values(input_data, frame_num, network):
    return get_coord_evaluation_values(input_data, frame_num, network, LEVEL_DEPTHS, LEVEL_FEATURE_NUM)

def get_nature_coord_evaluation_values(input_data, frame_num, network):
    return get_coord_evaluation_values(input_data, frame_num, network, NATURE_DEPTHS, NATURE_FEATURE_NUM)

def get_move_coord_evaluation_values(input_data, frame_num, network, depths):
    poke_names = input_data_to_poke_names(input_data)
    assert poke_names[frame_num] is not None
    assert poke_names[frame_num] != "なし"
    poke_name = poke_names[frame_num]

    result = get_coord_evaluation_values(input_data, frame_num, network, depths, MOVE_FEATURE_NUM)

    move_names = input_data_to_move_names(input_data, frame_num)

    for i, key in enumerate(result):
        if i == 0:
            continue

        if DATA_BASE_MOVE_NAMES[i] in move_names:
            result[key] = -999

        if DATA_BASE_MOVE_NAMES[i] not in seviper.POKEDEX[poke_name]["Learnset"]:
            result[key] = -999
    assert not all([value == -999 for value in result.values()])
    return result

def get_move1_coord_evaluation_values(input_data, frame_num, network):
    result = get_move_coord_evaluation_values(input_data, frame_num, network, MOVE1_DEPTHS)
    result[(0, 0, MOVE1_DEPTHS[frame_num])] = -999
    assert not all([value == -999 for value in result.values()])
    return result

def get_move2_coord_evaluation_values(input_data, frame_num, network):
    return get_move_coord_evaluation_values(input_data, frame_num, network, MOVE2_DEPTHS)

def get_move3_coord_evaluation_values(input_data, frame_num, network):
    return get_move_coord_evaluation_values(input_data, frame_num, network, MOVE3_DEPTHS)

def get_move4_coord_evaluation_values(input_data, frame_num, network):
    return get_move_coord_evaluation_values(input_data, frame_num, network, MOVE4_DEPTHS)

def get_iv_coord_evaluation_values(input_data, frame_num, network, depths):
    return get_coord_evaluation_values(input_data, frame_num, network, depths, IV_FEATURE_NUM)

def get_iv_hp_coord_evaluation_values(input_data, frame_num, network):
    return get_iv_coord_evaluation_values(input_data, frame_num, network, HP_IV_DEPTHS)

def get_iv_atk_coord_evaluation_values(input_data, frame_num, network):
    return get_iv_coord_evaluation_values(input_data, frame_num, network, ATK_IV_DEPTHS)

def get_iv_def_coord_evaluation_values(input_data, frame_num, network):
    return get_iv_coord_evaluation_values(input_data, frame_num, network, DEF_IV_DEPTHS)

def get_iv_sp_atk_coord_evaluation_values(input_data, frame_num, network):
    return get_iv_coord_evaluation_values(input_data, frame_num, network, SP_ATK_IV_DEPTHS)

def get_iv_sp_def_coord_evaluation_values(input_data, frame_num, network):
    return get_iv_coord_evaluation_values(input_data, frame_num, network, SP_DEF_IV_DEPTHS)

def get_iv_speed_coord_evaluation_values(input_data, frame_num, network):
    return get_iv_coord_evaluation_values(input_data, frame_num, network, SPEED_IV_DEPTHS)

def get_ev_coord_evaluation_values(input_data, frame_num, network, depth):
    result = get_coord_evaluation_values(input_data, frame_num, network, depth, EV_FEATURE_NUM)

    ev_stats = input_data_to_ev_stats(input_data, frame_num)
    ev_stats = [ev if ev is not None else 0 for ev in ev_stats]
    total_ev = sum(ev_stats)

    for i, key in enumerate(result):
        if (total_ev + i) > 510:
            result[key] = -999
    assert not all([value == -999 for value in result.values()])
    return result

def get_ev_hp_coord_evaluation_values(input_data, frame_num, network):
    return get_ev_coord_evaluation_values(input_data, frame_num, network, HP_EV_DEPTHS)

def get_ev_atk_coord_evaluation_values(input_data, frame_num, network):
    return get_ev_coord_evaluation_values(input_data, frame_num, network, ATK_EV_DEPTHS)

def get_ev_def_coord_evaluation_values(input_data, frame_num, network):
    return get_ev_coord_evaluation_values(input_data, frame_num, network, DEF_EV_DEPTHS)

def get_ev_sp_atk_coord_evaluation_values(input_data, frame_num, network):
    return get_ev_coord_evaluation_values(input_data, frame_num, network, SP_ATK_EV_DEPTHS)

def get_ev_sp_def_coord_evaluation_values(input_data, frame_num, network):
    return get_ev_coord_evaluation_values(input_data, frame_num, network, SP_DEF_EV_DEPTHS)

def get_ev_speed_coord_evaluation_values(input_data, frame_num, network):
    return get_ev_coord_evaluation_values(input_data, frame_num, network, SPEED_EV_DEPTHS)

def get_feature_coord_greedy_select(coord_evaluation_values, percent):
    evaluation_array = np.array(list(coord_evaluation_values.values()))

    if percent < random.randint(1, 100):
        index = crow.get_random_max_index(evaluation_array)
        return list(coord_evaluation_values.keys())[index], False

    evaluation_values = list(coord_evaluation_values.values())
    assert not all([value == -999 for value in evaluation_values])

    while True:
        index = random.randint(0, len(evaluation_array) - 1)
        if evaluation_values[index] != -999:
            return list(coord_evaluation_values.keys())[index], True
    assert False

def greedy_select(input_data, action_data, frame_nums, network, get_coord_evaluation_values_func,
                  random_percent_list):
    for frame_num in frame_nums:
        coord_evaluation_values = get_coord_evaluation_values_func(input_data, frame_num, network)
        random_percent = random_percent_list[frame_num]
        (h, w, d), _ = get_feature_coord_greedy_select(coord_evaluation_values, random_percent)
        input_data[h][w][d] = 1
        action_data.append(copy.deepcopy(input_data))

def team_build(random_num, network):
    input_data = get_init_input_data()
    action_data = []

    random_percent_list_of_list = [[0, 0, 0, 0, 0, 0] for _ in range(Network.DEPTH // 6)]

    for _ in range(random_num):
        while True:
            i = random.randint(0, len(random_percent_list_of_list) - 1)
            j = random.randint(0, 5)
            if random_percent_list_of_list[i][j] != 100:
                random_percent_list_of_list[i][j] = 100
                break

    greedy_select(input_data, action_data, [0, 1, 2, 3, 4, 5], network,
                  get_pokemon_coord_evaluation_values,
                  random_percent_list_of_list[0])

    poke_names = input_data_to_poke_names(input_data)
    assert None not in poke_names
    frame_nums = [i for i, poke_name in enumerate(poke_names) if poke_name != "なし"]

    funcs = [get_level_coord_evaluation_values, get_nature_coord_evaluation_values,
             get_move1_coord_evaluation_values, get_move2_coord_evaluation_values,
             get_move3_coord_evaluation_values, get_move4_coord_evaluation_values,
             get_iv_hp_coord_evaluation_values, get_iv_atk_coord_evaluation_values,
             get_iv_def_coord_evaluation_values, get_iv_sp_atk_coord_evaluation_values,
             get_iv_sp_def_coord_evaluation_values, get_iv_speed_coord_evaluation_values,
             get_ev_hp_coord_evaluation_values, get_ev_atk_coord_evaluation_values,
             get_ev_def_coord_evaluation_values, get_ev_sp_atk_coord_evaluation_values,
             get_ev_sp_def_coord_evaluation_values, get_ev_speed_coord_evaluation_values]

    assert len(funcs) == len(random_percent_list_of_list) - 1

    for i, func in enumerate(funcs):
        greedy_select(input_data, action_data, frame_nums, network, func,
                      random_percent_list_of_list[i + 1])
    return input_data, action_data

def input_data_to_team_info(input_data):
    poke_names = input_data_to_poke_names(input_data)
    levels = input_data_to_levels(input_data)
    nature_names = input_data_to_nature_names(input_data)
    move_names_list = [input_data_to_move_names(input_data, frame_num) for frame_num in range(6)]
    iv_stats_list = [input_data_to_iv_stats(input_data, frame_num) for frame_num in range(6)]
    ev_stats_list = [input_data_to_ev_stats(input_data, frame_num) for frame_num in range(6)]

    result = {}
    result["poke_names"] = poke_names
    result["levels"] = levels
    result["nature_names"] = nature_names

    for i in range(4):
        key = "move" + str(i + 1) + "_names"
        move_names = []
        for frame_num in range(6):
            move_names.append(move_names_list[frame_num][i])
        result[key] = move_names

    for i in range(6):
        keys = ["hp_ivs", "atk_ivs", "def_ivs", "sp_atk_ivs", "sp_def_ivs", "speed_ivs"]
        stats = []
        for frame_num in range(6):
            stats.append(iv_stats_list[frame_num][i])
        result[keys[i]] = stats

    for i in range(6):
        keys = ["hp_evs", "atk_evs", "def_evs", "sp_atk_evs", "sp_def_evs", "speed_evs"]
        stats = []
        for frame_num in range(6):
            stats.append(ev_stats_list[frame_num][i])
        result[keys[i]] = stats
    return result

if __name__ == "__main__":
    input_data = get_init_input_data()
    session = tf.Session()
    network = Network(session)
    session.run(tf.global_variables_initializer())
    input_data, action_data = team_build(3, network)
    for key, value in input_data_to_team_info(input_data).items():
        print(key, value)
