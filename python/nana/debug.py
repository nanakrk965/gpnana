import unittest
import nana.src as nana
from collections import OrderedDict
import tensorflow as tf

session = tf.Session()
network = nana.Network(session)
session.run(tf.global_variables_initializer())

class TestInputDataToPokeNames(unittest.TestCase):
    def test(self):
        input_data = nana.get_init_input_data()
        input_data[0][1][0] = 1.0
        input_data[0][2][1] = 1.0
        input_data[0][3][2] = 1.0
        input_data[0][0][3] = 1.0
        input_data[0][0][5] = 1.0

        result = nana.input_data_to_poke_names(input_data)
        self.assertEqual(result, ["フシギダネ", "ヒトカゲ", "ゼニガメ", "なし", None, "なし"])


class TestGetCoordEvaluationValues(unittest.TestCase):
    def test_pokemon(self):
        input_data = nana.get_init_input_data()

        coord_evaluation_values = nana.get_pokemon_coord_evaluation_values(input_data, 0, network)
        self.assertEqual(list(coord_evaluation_values.values())[0], -999)
        self.assertTrue(list(coord_evaluation_values.values())[1] != -999)
        self.assertTrue(list(coord_evaluation_values.values())[2] != -999)
        self.assertTrue(list(coord_evaluation_values.values())[3] != -999)

        input_data[0][3][0] = 1.0

        coord_evaluation_values = nana.get_pokemon_coord_evaluation_values(input_data, 1, network)
        self.assertEqual(list(coord_evaluation_values.values())[0], -999)
        self.assertTrue(list(coord_evaluation_values.values())[1] != -999)
        self.assertTrue(list(coord_evaluation_values.values())[2] != -999)
        self.assertEqual(list(coord_evaluation_values.values())[3], -999)


        input_data[0][2][1] = 1.0

        coord_evaluation_values = nana.get_pokemon_coord_evaluation_values(input_data, 2, network)
        self.assertEqual(list(coord_evaluation_values.values())[0], -999)
        self.assertTrue(list(coord_evaluation_values.values())[1] != -999)
        self.assertEqual(list(coord_evaluation_values.values())[2], -999)
        self.assertEqual(list(coord_evaluation_values.values())[3], -999)

        input_data[0][1][2] = 1.0

        for frame_num in [3, 4, 5]:
            coord_evaluation_values = nana.get_pokemon_coord_evaluation_values(input_data, frame_num, network)
            input_data[0][0][frame_num] = 1.0
            self.assertTrue(list(coord_evaluation_values.values())[0] != -999)
            self.assertEqual(list(coord_evaluation_values.values())[1], -999)
            self.assertEqual(list(coord_evaluation_values.values())[2], -999)
            self.assertEqual(list(coord_evaluation_values.values())[3], -999)


class TestGetFeatureCoordGreedySelect(unittest.TestCase):
    def test_random_30percent_pattern(self):
        coord_evaluation_values = OrderedDict()
        coord_evaluation_values[(0, 0, 7)] = 1.0
        coord_evaluation_values[(0, 1, 7)] = 0.5
        coord_evaluation_values[(0, 2, 7)] = 1.0

        random_count = 0
        results_count = [0, 0, 0]
        for i in range(1000):
            result, is_random = nana.get_feature_coord_greedy_select(coord_evaluation_values, 30)
            assert result[0] == 0
            assert result[2] == 7

            if is_random:
                random_count += 1
            else:
                results_count[result[1]] += 1

        print(random_count, ":300付近であればテスト成功")
        print(results_count, ":中央は0 両端は値が均衡しているならテスト成功")
        print(results_count[0] + results_count[2], ":700付近であればテスト成功")

    def test_random_100percent_pattern(self):
        coord_evaluation_values = OrderedDict()
        coord_evaluation_values[(0, 0, 0)] = 1.0
        coord_evaluation_values[(0, 1, 0)] = 0.5
        coord_evaluation_values[(0, 2, 0)] = 1.0

        random_count = 0
        roop_num = 1000

        for i in range(roop_num):
            result, is_random = nana.get_feature_coord_greedy_select(coord_evaluation_values, 100)
            if is_random:
                random_count += 1
        self.assertEqual(random_count, roop_num)

    def test_not_random_pattern(self):
        coord_evaluation_values = OrderedDict()
        coord_evaluation_values[(0, 0, 7)] = 0.5
        coord_evaluation_values[(0, 1, 7)] = 1.0
        coord_evaluation_values[(0, 2, 7)] = 0.75

        random_count = 0
        results_count = [0, 0, 0]
        roop_num = 1000

        for i in range(roop_num):
            result, is_random = nana.get_feature_coord_greedy_select(coord_evaluation_values, 0)

            if is_random:
                random_count += 1
            results_count[result[1]] += 1

        self.assertEqual(random_count, 0)
        self.assertEqual(results_count[0], 0)
        self.assertEqual(results_count[1], roop_num)
        self.assertEqual(results_count[2], 0)


class TestTeamBuild(unittest.TestCase):
    def test(self):
        input_data, action_data = nana.team_build(5, network)
        results = []
        for coord in nana.get_all_2d_coords(nana.Network.HEIGTH * nana.Network.WIDTH):
            for depth in range(nana.Network.DEPTH):
                h, w = coord
                results.append(input_data[h][w][depth] == action_data[-1][h][w][depth])
        self.assertTrue(all(results))

        for i, ele in enumerate(action_data):
            self.assertEqual(sum([sum(ele2) for ele1 in ele for ele2 in ele1]), i + 1)

        self.assertEqual(len(action_data), 3 + (nana.Network.DEPTH / 2))

    def test10(self):
        for i in range(10):
            self.test()

if __name__ == "__main__":
    unittest.main()
