package bippa

import (
  "testing"
  "fmt"
  "math/rand"
  "time"
  "github.com/seehuhn/mt19937"
)

func NewHusigidane() Pokemon {
  husigidaneName, err := NewPokeName("フシギダネ")

  if err != nil {
    panic(err)
  }

  husigidaneLevel, err := NewLevel(50)

  if err != nil {
    panic(err)
  }

  husigidaneNature, err := NewNatureName("いじっぱり")

  if err != nil {
    panic(err)
  }

  husigidaneMoveset, err := NewMoveset(&husigidaneName, []string{"たいあたり", "つるのムチ"})

  if err != nil {
    panic(err)
  }

  husigidaneIVStats, err := NewIVStats(31, 31, 31, 31, 31, 31)

  if err != nil {
    panic(err)
  }

  husigidaneEVStats, err := NewEVStats(252, 252, 0, 0, 0, 4)

  if err != nil {
    panic(err)
  }

  return NewPokemon(&husigidaneName, &husigidaneLevel, &husigidaneNature,
                    &husigidaneMoveset, &husigidaneIVStats, &husigidaneEVStats)
}

func NewHitokage() Pokemon {
  hitokageName, err := NewPokeName("ヒトカゲ")

  if err != nil {
    panic(err)
  }

  hitokageLevel, err := NewLevel(50)

  if err != nil {
    panic(err)
  }

  hitokageNature, err := NewNatureName("ひかえめ")

  if err != nil {
    panic(err)
  }

  hitokageMoveset, err := NewMoveset(&hitokageName, []string{"ひのこ"})

  if err != nil {
    panic(err)
  }

  hitokageIVStats, err := NewIVStats(31, 0, 31, 31, 31, 31)

  if err != nil {
    panic(err)
  }

  hitokageEVStats, err := NewEVStats(252, 0, 0, 252, 0, 4)

  if err != nil {
    panic(err)
  }

  return NewPokemon(&hitokageName, &hitokageLevel, &hitokageNature,
                    &hitokageMoveset, &hitokageIVStats, &hitokageEVStats)
}

func NewZenigame() Pokemon {
  zenigameName, err := NewPokeName("ゼニガメ")

  if err != nil {
    panic(err)
  }

  zenigameLevel, err := NewLevel(50)

  if err != nil {
    panic(err)
  }

  zenigameNature, err := NewNatureName("おだやか")

  if err != nil {
    panic(err)
  }

  zenigameMoveset, err := NewMoveset(&zenigameName, []string{"みずでっぽう"})

  if err != nil {
    panic(err)
  }

  zenigameIVStats, err := NewIVStats(31, 0, 31, 31, 31, 31)

  if err != nil {
    panic(err)
  }

  zenigameEVStats, err := NewEVStats(252, 0, 0, 0, 252, 4)

  if err != nil {
    panic(err)
  }

  return NewPokemon(&zenigameName, &zenigameLevel, &zenigameNature,
                    &zenigameMoveset, &zenigameIVStats, &zenigameEVStats)
}

func NewTemplateFighters() Fighters {
  teamValue := []Pokemon{NewHusigidane(), NewHitokage(), NewZenigame()}
  team, err := NewTeam(teamValue)

  if err != nil {
    panic(err)
  }

  result, err := team.ChooseFighters(&[3]int{0, 1, 2})

  if err != nil {
    panic(err)
  }

  return result
}

func NewTemplateAspect() Aspect {
  selfTeamValue := []Pokemon{NewHusigidane(), NewHitokage(), NewZenigame()}
  selfTeam, err := NewTeam(selfTeamValue)

  if err != nil {
    panic(err)
  }

  selfFighters, err := selfTeam.ChooseFighters(&[3]int{0, 1, 2})

  if err != nil {
    panic(err)
  }

  opponentTeamValue := []Pokemon{NewZenigame(), NewHitokage(), NewHusigidane()}
  opponentTeam, err := NewTeam(opponentTeamValue)

  if err != nil {
    panic(err)
  }

  opponentFighters, err := opponentTeam.ChooseFighters(&[3]int{0, 1, 2})

  if err != nil {
    panic(err)
  }
  return Aspect{Self:selfFighters, Opponent:opponentFighters}
}

func TestNewHusigidane(t *testing.T) {
  husigidane := NewHusigidane()

  if husigidane.Name.value != "フシギダネ" {
    t.Errorf("テスト失敗")
  }

  if husigidane.Level.value != 50 {
    t.Errorf("テスト失敗")
  }

  if husigidane.Nature.value != "いじっぱり" {
    t.Errorf("テスト失敗")
  }

  if len(husigidane.Moveset.value) != 2 {
    t.Errorf("テスト失敗")
  }

  for i, strMoveName := range []string{"たいあたり", "つるのムチ"} {
    moveName := husigidane.Moveset.value[i]
    if moveName.value != strMoveName {
      t.Errorf("テスト失敗")
    }
  }

  if husigidane.MaxHP != 152 {
    t.Errorf("テスト失敗")
  }

  if husigidane.CurrentHP != 152 {
    t.Errorf("テスト失敗")
  }

  if husigidane.Atk != 111 {
    t.Errorf("テスト失敗")
  }

  if husigidane.Def != 69 {
    t.Errorf("テスト失敗")
  }

  if husigidane.SpAtk != 76 {
    t.Errorf("テスト失敗")
  }

  if husigidane.SpDef != 85 {
    t.Errorf("テスト失敗")
  }

  if husigidane.Speed != 66 {
    t.Errorf("テスト失敗")
  }
}

func TestNewHitokage(t *testing.T) {
  hitokage := NewHitokage()

  if hitokage.Name.value != "ヒトカゲ" {
    t.Errorf("テスト失敗")
  }

  if hitokage.Level.value != 50 {
    t.Errorf("テスト失敗")
  }

  if hitokage.Nature.value != "ひかえめ" {
    t.Errorf("テスト失敗")
  }

  if len(hitokage.Moveset.value) != 1 {
    t.Errorf("テスト失敗")
  }

  for i, strMoveName := range []string{"ひのこ"} {
    moveName := hitokage.Moveset.value[i]
    if moveName.value != strMoveName {
      t.Errorf("テスト失敗")
    }
  }

  if hitokage.MaxHP != 146 {
    t.Errorf("テスト失敗")
  }

  if hitokage.CurrentHP != 146 {
    t.Errorf("テスト失敗")
  }

  if hitokage.Atk != 51 {
    t.Errorf("テスト失敗")
  }

  if hitokage.Def != 63 {
    t.Errorf("テスト失敗")
  }

  if hitokage.SpAtk != 123 {
    t.Errorf("テスト失敗")
  }

  if hitokage.SpDef != 70 {
    t.Errorf("テスト失敗")
  }

  if hitokage.Speed != 86 {
    errMsg := fmt.Sprintf("hitokage.Speed == %d", hitokage.Speed)
    t.Errorf(errMsg)
  }
}

func TestNewZenigame(t *testing.T) {
  zenigame := NewZenigame()

  if zenigame.Name.value != "ゼニガメ" {
    t.Errorf("テスト失敗")
  }

  if zenigame.Level.value != 50 {
    t.Errorf("テスト失敗")
  }

  if zenigame.Nature.value != "おだやか" {
    t.Errorf("テスト失敗")
  }

  if len(zenigame.Moveset.value) != 1 {
    t.Errorf("テスト失敗")
  }

  for i, strMoveName := range []string{"みずでっぽう"} {
    moveName := zenigame.Moveset.value[i]
    if moveName.value != strMoveName {
      t.Errorf("テスト失敗")
    }
  }

  if zenigame.MaxHP != 151 {
    t.Errorf("テスト失敗")
  }

  if zenigame.CurrentHP != 151 {
    t.Errorf("テスト失敗")
  }

  if zenigame.Atk != 47 {
    t.Errorf("テスト失敗")
  }

  if zenigame.Def != 85 {
    t.Errorf("テスト失敗")
  }

  if zenigame.SpAtk != 70 {
    t.Errorf("テスト失敗")
  }

  if zenigame.SpDef != 127 {
    t.Errorf("テスト失敗")
  }

  if zenigame.Speed != 64 {
    t.Errorf("テスト失敗")
  }
}

func TestGetLegalCommandsAllNotFaintPattern(t *testing.T) {
  fighters := NewTemplateFighters()
  result := fighters.GetLegalCommands()
  expectedIntCommands := []int{0, 1, 4, 5}

  if len(result) != len(expectedIntCommands) {
    t.Errorf("テスト失敗")
  }

  for i, intCommand := range expectedIntCommands {
    if result[i].value != intCommand {
      t.Errorf("テスト失敗")
    }
  }
}

func TestGetLegalCommandsFirstFaintPattern(t *testing.T) {
  fighters := NewTemplateFighters()
  fighters.value[0].CurrentHP = 0
  result := fighters.GetLegalCommands()
  expectedIntCommands := []int{4, 5}

  if len(result) != len(expectedIntCommands) {
    t.Errorf("テスト失敗")
  }

  for i, intCommand := range expectedIntCommands {
    if result[i].value != intCommand {
      t.Errorf("テスト失敗")
    }
  }
}

func TestGetLegalCommandsThirdPattern(t *testing.T) {
  fighters := NewTemplateFighters()
  fighters.value[2].CurrentHP = 0
  result := fighters.GetLegalCommands()
  expectedIntCommands := []int{0, 1, 4}

  if len(result) != len(expectedIntCommands) {
    t.Errorf("テスト失敗")
  }

  for i, intCommand := range expectedIntCommands {
    if result[i].value != intCommand {
      t.Errorf("テスト失敗")
    }
  }
}

func TestSelectFighters(t *testing.T) {
  teamValue := []Pokemon{NewZenigame(), NewHitokage(), NewHusigidane()}
  team, err := NewTeam(teamValue)

  if err != nil {
    panic(err)
  }

  fighters, err := team.ChooseFighters(&[3]int{2, 1, 0})

  if err != nil {
    panic(err)
  }

  if fighters.value[0].Name.value != "フシギダネ" {
    t.Errorf("テスト失敗")
  }

  if fighters.value[1].Name.value != "ヒトカゲ" {
    t.Errorf("テスト失敗")
  }

  if fighters.value[2].Name.value != "ゼニガメ" {
    t.Errorf("テスト失敗")
  }
}

func TestGetActionOrderPriorityWinnerOpponentPattern(t *testing.T) {
  aspect := NewTemplateAspect()

  mtRandom := rand.New(mt19937.New())
  mtRandom.Seed(time.Now().UnixNano())

  bothCommand, err := NewBothCommand(&aspect, 0, 4)

  if err != nil {
    panic(err)
  }

  actionOrder := aspect.GetActionOrder(&bothCommand, mtRandom)

  if actionOrder[0].IsSelf {
    t.Errorf("テスト失敗")
  }

  if !actionOrder[1].IsSelf {
    t.Errorf("テスト失敗")
  }
}

func TestGetActionOrderSpeedWinnerSelfPattern(t *testing.T) {
  aspect := NewTemplateAspect()

  mtRandom := rand.New(mt19937.New())
  mtRandom.Seed(time.Now().UnixNano())

  bothCommand, err := NewBothCommand(&aspect, 1, 0)

  if err != nil {
    panic(err)
  }

  actionOrder := aspect.GetActionOrder(&bothCommand, mtRandom)

  if !actionOrder[0].IsSelf {
    t.Errorf("テスト失敗")
  }

  if actionOrder[1].IsSelf {
    t.Errorf("テスト失敗")
  }
}

func TestGetActionOrderDrawPattern(t *testing.T) {
  aspect := NewTemplateAspect()

  mtRandom := rand.New(mt19937.New())
  mtRandom.Seed(time.Now().UnixNano())

  switchCommand, err := NewSwitchCommand(&aspect.Self, 5)

  if err != nil {
    panic(err)
  }

  aspect.Self = aspect.Self.Switch(&switchCommand)

  if aspect.Self.value[0].Name.value != "ゼニガメ" {
    t.Errorf("テスト失敗")
  }

  bothCommand, err := NewBothCommand(&aspect, 0, 0)

  selfFirstCount := 0
  opponentFirstCount := 0

  for i := 0; i < 1280; i++ {
    actionOrder := aspect.GetActionOrder(&bothCommand, mtRandom)
    if actionOrder[0].IsSelf {
      selfFirstCount += 1
    }

    if !actionOrder[0].IsSelf {
      opponentFirstCount += 1
    }
  }

  msg := fmt.Sprintf("selfFirstCount = %d opponentFirstCount = %d (値が近ければテスト成功)",
                     selfFirstCount, opponentFirstCount)
  t.Errorf(msg)
}

func TestActionMoveUsePattern(t *testing.T) {
  aspect := NewTemplateAspect()

  selfCommand, err := NewCommand(&aspect.Self, 0)

  if err != nil {
    panic(err)
  }

  aspect = aspect.Action(&selfCommand)
  expectedCurrentHP := 151 - 24

  if aspect.Opponent.value[0].CurrentHP != expectedCurrentHP {
    t.Errorf("テスト失敗")
  }

  selfCommand, err = NewCommand(&aspect.Self, 1)

  if err != nil {
    panic(err)
  }

  aspect = aspect.Action(&selfCommand)
  expectedCurrentHP -= 80

  if aspect.Opponent.value[0].CurrentHP != expectedCurrentHP {
    t.Errorf("テスト失敗")
  }
}

func TestOpponentActionSwitchPattern(t *testing.T) {
  aspect := NewTemplateAspect()

  opponentCommand, err := NewCommand(&aspect.Opponent, 4)

  if err != nil {
    panic(err)
  }

  aspect = aspect.OpponentAction(&opponentCommand)

  for i, strPokeName := range []string{"ヒトカゲ", "ゼニガメ", "フシギダネ"} {
    if aspect.Opponent.value[i].Name.value != strPokeName {
      t.Errorf("テスト失敗")
    }
  }

  opponentCommand, err = NewCommand(&aspect.Opponent, 5)

  if err != nil {
    panic(err)
  }

  aspect = aspect.OpponentAction(&opponentCommand)

  for i, strPokeName := range []string{"フシギダネ", "ゼニガメ", "ヒトカゲ"} {
    if aspect.Opponent.value[i].Name.value != strPokeName {
      t.Errorf("テスト失敗")
    }
  }
}

func TestPlayoutSelfAdvantageousAspect(t *testing.T) {
  mtRandom := rand.New(mt19937.New())
  mtRandom.Seed(time.Now().UnixNano())
  aspect := NewTemplateAspect()

  for i := 0; i < FIGHTERS_LENGTH; i++ {
    aspect.Opponent.value[i].CurrentHP /= 2
  }

  winRate := aspect.Playout(mtRandom, 12800)

  errMsg := fmt.Sprintf("0.9付近であればテスト成功 : %g", winRate)
  t.Errorf(errMsg)
}

func TestPlayoutOpponentAdvantageousAspect(t *testing.T) {
  mtRandom := rand.New(mt19937.New())
  mtRandom.Seed(time.Now().UnixNano())
  aspect := NewTemplateAspect()

  aspect.Self.value[2].CurrentHP = 0

  winRate := aspect.Playout(mtRandom, 12800)

  errMsg := fmt.Sprintf("0.3付近であればテスト成功 : %g", winRate)
  t.Errorf(errMsg)
}
