package bippa

import (
  "fmt"
  "os"
  "io/ioutil"
  "encoding/json"
  "math/rand"
  "omw"
)

var DATA_PATH = os.Getenv("GOPATH") + "/src/gpnana/go/bippa/data/"
var POKEDEX_PATH = DATA_PATH + "pokedex.json"
var MOVEDEX_PATH = DATA_PATH + "movedex.json"
var NATUREDEX_PATH = DATA_PATH + "naturedex.json"
var TYPEDEX_PATH = DATA_PATH + "typedex.json"

type PokeData struct {
  ID int
  Types []string
  BaseHP int
  BaseAtk int
  BaseDef int
  BaseSpAtk int
  BaseSpDef int
  BaseSpeed int
  Learnset []string
}

type Pokedex map[string]PokeData

var POKEDEX = func() Pokedex {
  bytes, err := ioutil.ReadFile(POKEDEX_PATH)

  if err != nil {
    panic(err)
  }

  result := Pokedex{}

  if err := json.Unmarshal(bytes, &result); err != nil {
    panic(err)
  }
  return result
}()

type MoveData struct {
  ID int
  Type string
  Category string
  Power int
  Accuracy int
}

type Movedex map[string]MoveData

var MOVEDEX = func() Movedex {
  bytes, err := ioutil.ReadFile(MOVEDEX_PATH)

  if err != nil {
    panic(err)
  }

  result := Movedex{}

  if err := json.Unmarshal(bytes, &result); err != nil {
    panic(err)
  }
  return result
}()

type NatureData struct {
  ID int
  AtkBonus float64
  DefBonus float64
  SpAtkBonus float64
  SpDefBonus float64
  SpeedBonus float64
}

type Naturedex map[string]NatureData

var NATUREDEX = func() Naturedex {
  bytes, err := ioutil.ReadFile(NATUREDEX_PATH)

  if err != nil {
    panic(err)
  }

  result := Naturedex{}

  if err := json.Unmarshal(bytes, &result); err != nil {
    panic(err)
  }
  return result
}()

type TypeData map[string]float64
type Typedex map[string]TypeData

var TYPEDEX = func() Typedex {
  bytes, err := ioutil.ReadFile(TYPEDEX_PATH)

  if err != nil {
    panic(err)
  }

  result := Typedex{}

  if err := json.Unmarshal(bytes, &result); err != nil {
    panic(err)
  }
  return result
}()

//正しいポケモン名以外入力出来ないようにアクセス制限
type PokeName struct {
  value string
}

const POKE_NAME_ERR_MSG = "ポケモン名が不正"

func NewPokeName(value string) (PokeName, error) {
  if _, ok := POKEDEX[value]; ok {
    return PokeName{value:value}, nil
  }
  return PokeName{}, fmt.Errorf(POKE_NAME_ERR_MSG)
}

//1～50以外入力出来ないようにアクセス制限
type Level struct {
  value int
}

const MIN_LEVEL = 1
const MAX_LEVEL = 50

func NewLevel(value int) (Level, error) {
  if value >= MIN_LEVEL && value <= MAX_LEVEL {
    return Level{value:value}, nil
  }
  errMsg := fmt.Sprintf("レベルは%d～%dでなければならない", MIN_LEVEL, MAX_LEVEL)
  return Level{}, fmt.Errorf(errMsg)
}

//POKEDEXに存在する種族値以外入力出来ないようにアクセス制限
type BaseStats struct {
  value int
}

const BASE_STATS_ERR_MSG = "種族値が不正"

func NewBaseStats(value int) (BaseStats, error) {
  for _, pokeData := range POKEDEX {
    if pokeData.BaseHP == value {
      return BaseStats{value:value}, nil
    }

    if pokeData.BaseAtk == value {
      return BaseStats{value:value}, nil
    }

    if pokeData.BaseDef == value {
      return BaseStats{value:value}, nil
    }

    if pokeData.BaseSpAtk == value {
      return BaseStats{value:value}, nil
    }

    if pokeData.BaseSpDef == value {
      return BaseStats{value:value}, nil
    }

    if pokeData.BaseSpeed == value {
      return BaseStats{value:value}, nil
    }
  }
  return BaseStats{}, fmt.Errorf(BASE_STATS_ERR_MSG)
}

//正しい性格名以外入力出来ないようにアクセス制限
type NatureName struct {
  value string
}

const NATURE_NAME_ERR_MSG = "性格名が不正"

func NewNatureName(value string) (NatureName, error) {
  if _, ok := NATUREDEX[value]; ok {
    return NatureName{value:value}, nil
  }
  return NatureName{}, fmt.Errorf(NATURE_NAME_ERR_MSG)
}

//0.9 / 1.0 / 1.1 以外入力出来ないようにアクセス制限
type NatureBonus struct {
  value float64
}

const NATURE_BONUS_ERR_MSG = "性格補正は 0.9 / 1.0 / 1.1 のいずれかでなければならない"

func NewNatureBonus(value float64) (NatureBonus, error) {
  if value == 0.9 || value == 1.0 || value == 1.1 {
    return NatureBonus{value:value}, nil
  }
  return NatureBonus{}, fmt.Errorf(NATURE_BONUS_ERR_MSG)
}

//正しい技名以外入力出来ないようにアクセス制限
type MoveName struct {
  value string
}

const MOVE_NAME_ERR_MSG = "技名が不正"

func NewMoveName(value string) (MoveName, error) {
  if _, ok := MOVEDEX[value]; ok {
    return MoveName{value:value}, nil
  }
  return MoveName{}, fmt.Errorf(MOVE_NAME_ERR_MSG)
}

func InLearnset(pokeName *PokeName, moveName *MoveName) bool {
  for _, strMoveName := range POKEDEX[pokeName.value].Learnset {
    if moveName.value == strMoveName {
      return true
    }
  }
  return false
}

//覚える技以外入力出来ないようにアクセス制限
type Moveset struct {
  value []MoveName
}

const MIN_MOVESET_LENGTH = 1
const MAX_MOVESET_LENGTH = 4

func NewMoveset(pokeName *PokeName, value []string) (Moveset, error) {
  if !(len(value) >= MIN_MOVESET_LENGTH && len(value) <= MAX_MOVESET_LENGTH) {
    errMsg := fmt.Sprintf("覚えさせる技の数は%d～%dでなければならない", MIN_MOVESET_LENGTH, MAX_MOVESET_LENGTH)
    return Moveset{}, fmt.Errorf(errMsg)
  }

  tmpValue := make([]MoveName, len(value))

  for i, ele := range value {
    moveName, err := NewMoveName(ele)
    if err != nil {
      return Moveset{}, err
    }
    tmpValue[i] = moveName
  }

  for _, moveName := range tmpValue {
    if !InLearnset(pokeName, &moveName) {
      errMsg := fmt.Sprintf("%s は %s を 覚えない", pokeName.value, moveName.value)
      return Moveset{}, fmt.Errorf(errMsg)
    }
  }
  return Moveset{value:tmpValue}, nil
}

//0～31以外入力出来ないようにアクセス制限
type IV struct {
  value int
}

const MIN_IV = 0
const MAX_IV = 31

func NewIV(value int) (IV, error) {
  if value >= MIN_IV && value <= MAX_IV {
    return IV{value:value}, nil
  }
  errMsg := fmt.Sprintf("個体値は%d～%dでなければならない", MIN_IV, MAX_IV)
  return IV{}, fmt.Errorf(errMsg)
}

//0～31以外入力出来ないようにアクセス制限
type IVStats struct {
  hp IV
  atk IV
  def IV
  spAtk IV
  spDef IV
  speed IV
}

const (
  HP_IV_ERR_MSG = "HP個体値が不正"
  ATK_IV_ERR_MSG = "攻撃個体値が不正"
  DEF_IV_ERR_MSG = "防御個体値が不正"
  SP_ATK_IV_ERR_MSG = "特攻個体値が不正"
  SP_DEF_IV_ERR_MSG = "特防個体値が不正"
  SPEED_IV_ERR_MSG = "素早さ個体値が不正"
)

func NewIVStats(hp, atk, def, spAtk, spDef, speed int) (IVStats, error) {
  hpIV, err := NewIV(hp)
  if err != nil {
    return IVStats{}, fmt.Errorf(HP_IV_ERR_MSG)
  }

  atkIV, err := NewIV(atk)
  if err != nil {
    return IVStats{}, fmt.Errorf(ATK_IV_ERR_MSG)
  }

  defIV, err := NewIV(def)
  if err != nil {
    return IVStats{}, fmt.Errorf(DEF_IV_ERR_MSG)
  }

  spAtkIV, err := NewIV(spAtk)
  if err != nil {
    return IVStats{}, fmt.Errorf(SP_ATK_IV_ERR_MSG)
  }

  spDefIV, err := NewIV(spDef)
  if err != nil {
    return IVStats{}, fmt.Errorf(SP_DEF_IV_ERR_MSG)
  }

  speedIV, err := NewIV(speed)
  if err != nil {
    return IVStats{}, fmt.Errorf(SPEED_IV_ERR_MSG)
  }
  return IVStats{hp:hpIV, atk:atkIV, def:defIV, spAtk:spAtkIV, spDef:spDefIV, speed:speedIV}, nil
}

//0～252以外入力出来ないようにアクセス制限
type EV struct {
  value int
}

const MIN_EV = 0
const MAX_EV = 252

func NewEV(value int) (EV, error) {
  if value >= MIN_EV && value <= MAX_EV {
    return EV{value:value}, nil
  }
  errMsg := fmt.Sprintf("努力値は%d～%dでなければならない", MIN_EV, MAX_EV)
  return EV{}, fmt.Errorf(errMsg)
}

//合計510以上にならないように、アクセス制限
type EVStats struct {
  hp EV
  atk EV
  def EV
  spAtk EV
  spDef EV
  speed EV
}

const EV_HP_ERR_MSG = "HP努力値が不正"
const EV_ATK_ERR_MSG = "攻撃努力値が不正"
const EV_DEF_ERR_MSG = "防御努力値が不正"
const EV_SP_ATK_ERR_MSG = "特攻努力値が不正"
const EV_SP_DEF_ERR_MSG = "特防努力値が不正"
const EV_SPEED_ERR_MSG = "素早さ努力値が不正"
const TOTAL_EV_ERR_MSG = "合計努力値は0～510でなければならない"

func NewEVStats(hp, atk, def, spAtk, spDef, speed int) (EVStats, error) {
  hpEV, err := NewEV(hp)
  if err != nil {
    return EVStats{}, fmt.Errorf(EV_HP_ERR_MSG)
  }

  atkEV, err := NewEV(atk)
  if err != nil {
    return EVStats{}, fmt.Errorf(EV_ATK_ERR_MSG)
  }

  defEV, err := NewEV(def)
  if err != nil {
    return EVStats{}, fmt.Errorf(EV_DEF_ERR_MSG)
  }

  spAtkEV, err := NewEV(spAtk)
  if err != nil {
    return EVStats{}, fmt.Errorf(EV_SP_ATK_ERR_MSG)
  }

  spDefEV, err := NewEV(spDef)
  if err != nil {
    return EVStats{}, fmt.Errorf(EV_SP_DEF_ERR_MSG)
  }

  speedEV, err := NewEV(speed)
  if err != nil {
    return EVStats{}, fmt.Errorf(EV_SPEED_ERR_MSG)
  }

  sum := hp + atk + def + spAtk + spDef + speed
  if sum <= 510 {
    return EVStats{hp:hpEV, atk:atkEV, def:defEV, spAtk:spAtkEV, spDef:spDefEV, speed:speedEV}, nil
  }
  return EVStats{}, fmt.Errorf(TOTAL_EV_ERR_MSG)
}

func HpStatsFormula(level *Level, baseStats *BaseStats, iv *IV, ev *EV) int {
  return int((baseStats.value * 2 + iv.value + int(ev.value / 4)) * level.value / 100) + 10 + level.value
}

func StatsFormula(level *Level, baseStats *BaseStats, iv *IV, ev *EV, natureBonus *NatureBonus) int {
  result := int(int(baseStats.value * 2 + iv.value + int(ev.value / 4)) * level.value / 100) + 5
  return int(float64(result) * natureBonus.value)
}

type Pokemon struct {
  Name PokeName
  Level Level
  Nature NatureName
  Moveset Moveset

  MaxHP int
  CurrentHP int
  Atk int
  Def int
  SpAtk int
  SpDef int
  Speed int
}

const NEW_POKEMON_PANIC_MSG = "NewPokemonでライブラリのバグが発生した"

func NewPokemon(pokeName *PokeName, level *Level, natureName *NatureName,
                moveset *Moveset, ivStats *IVStats, evStats *EVStats) Pokemon {
  result := Pokemon{}

  result.Name = *pokeName
  result.Level = *level
  result.Nature = *natureName
  result.Moveset = *moveset

  pokeData := POKEDEX[pokeName.value]

  baseHP, err := NewBaseStats(pokeData.BaseHP)

  if err != nil {
    panic(NEW_POKEMON_PANIC_MSG)
  }

  baseAtk, err := NewBaseStats(pokeData.BaseAtk)

  if err != nil {
    panic(NEW_POKEMON_PANIC_MSG)
  }

  baseDef, err := NewBaseStats(pokeData.BaseDef)

  if err != nil {
    panic(NEW_POKEMON_PANIC_MSG)
  }

  baseSpAtk, err := NewBaseStats(pokeData.BaseSpAtk)

  if err != nil {
    panic(NEW_POKEMON_PANIC_MSG)
  }

  baseSpDef, err := NewBaseStats(pokeData.BaseSpDef)

  if err != nil {
    panic(NEW_POKEMON_PANIC_MSG)
  }

  baseSpeed, err := NewBaseStats(pokeData.BaseSpeed)

  if err != nil {
    panic(NEW_POKEMON_PANIC_MSG)
  }

  result.MaxHP = HpStatsFormula(level, &baseHP, &ivStats.hp, &evStats.hp)
  result.CurrentHP = result.MaxHP

  natureData := NATUREDEX[natureName.value]

  natureAtkBonus, err := NewNatureBonus(natureData.AtkBonus)

  if err != nil {
    panic(NEW_POKEMON_PANIC_MSG)
  }

  natureDefBonus, err := NewNatureBonus(natureData.DefBonus)

  if err != nil {
    panic(NEW_POKEMON_PANIC_MSG)
  }

  natureSpAtkBonus, err := NewNatureBonus(natureData.SpAtkBonus)

  if err != nil {
    panic(NEW_POKEMON_PANIC_MSG)
  }

  natureSpDefBonus, err := NewNatureBonus(natureData.SpDefBonus)

  if err != nil {
    panic(NEW_POKEMON_PANIC_MSG)
  }

  natureSpeedBonus, err := NewNatureBonus(natureData.SpeedBonus)

  if err != nil {
    panic(NEW_POKEMON_PANIC_MSG)
  }

  result.Atk = StatsFormula(level, &baseAtk, &ivStats.atk, &evStats.atk, &natureAtkBonus)
  result.Def = StatsFormula(level, &baseDef, &ivStats.def, &evStats.def, &natureDefBonus)
  result.SpAtk = StatsFormula(level, &baseSpAtk, &ivStats.spAtk, &evStats.spAtk, &natureSpAtkBonus)
  result.SpDef = StatsFormula(level, &baseSpDef, &ivStats.spDef, &evStats.spDef, &natureSpDefBonus)
  result.Speed = StatsFormula(level, &baseSpeed, &ivStats.speed, &evStats.speed, &natureSpeedBonus)
  return result
}

//長さが3～6以外にならないように / ポケモンが重複しないようにアクセス制限
type Team struct {
  value []Pokemon
}

func (team *Team) GetPokeNames() []PokeName {
  result := make([]PokeName, len(team.value))
  for i, pokemon := range team.value {
    result[i] = pokemon.Name
  }
  return result
}

func (team *Team) PokeNameCount(pokeName *PokeName) int {
  teamPokeNames := team.GetPokeNames()
  result := 0
  for _, tmpPokeName := range teamPokeNames {
    if pokeName.value == tmpPokeName.value {
      result += 1
    }
  }
  return result
}

func (team *Team) IsUnique() bool {
  teamPokeNames := team.GetPokeNames()
  for _, pokeName := range teamPokeNames {
    if team.PokeNameCount(&pokeName) != 1 {
      return false
    }
  }
  return true
}

const TEAM_MIN_LENGTH = 3
const TEAM_MAX_LENGTH = 6
const NOT_UNIQUE_TEAM_POKEMONS_ERR = "チームに同じ種類のポケモンを入れてはならない"

func NewTeam(value []Pokemon) (Team, error) {
  if !(len(value) >= TEAM_MIN_LENGTH && len(value) <= TEAM_MAX_LENGTH) {
    errMsg := fmt.Sprintf("チームは%d匹～%d匹で構成しなければならない", TEAM_MIN_LENGTH, TEAM_MAX_LENGTH)
    return Team{}, fmt.Errorf(errMsg)
  }

  team := Team{value:value}

  if !team.IsUnique() {
    return Team{}, fmt.Errorf(NOT_UNIQUE_TEAM_POKEMONS_ERR)
  }
  return team, nil
}

const FIGHTERS_LENGTH = 3
const NOT_UNIQUE_SELECT_INDICES = "同じポケモンは選出出来ない"

func (team *Team) ChooseFighters(indices *[FIGHTERS_LENGTH]int) (Fighters, error) {
  if !omw.IsUniqueSliceInt(indices[:]) {
    return Fighters{}, fmt.Errorf(NOT_UNIQUE_SELECT_INDICES)
  }

  var tmpValue [FIGHTERS_LENGTH]Pokemon

  for i, index := range indices {
    tmpValue[i] = team.value[index]
  }

  result, err := NewFighters(tmpValue)

  if err != nil {
    return Fighters{}, err
  }
  return result, nil
}

var commandCount = omw.Count{Value:0}

//合法な技使用コマンド以外入力出来ないようにアクセス制限
type MoveUseCommand struct {
  value int
}

var INT_MOVE_USE_COMMANDS = []int{commandCount.Value, commandCount.GetIncrement(),
                                  commandCount.GetIncrement(), commandCount.GetIncrement()}

//合法な交代コマンド以外入力出来ないようにアクセス制限
type SwitchCommand struct {
  value int
}

var INT_SWITCH_COMMANDS = []int{commandCount.GetIncrement(), commandCount.GetIncrement()}

//合法なコマンド以外入力出来ないようにアクセス制限
type Command struct {
  value int
}

var INT_COMMANDS = func() []int {
  result := make([]int, 0, len(INT_MOVE_USE_COMMANDS) + len(INT_SWITCH_COMMANDS))

  for _, intCommand := range INT_MOVE_USE_COMMANDS {
    result = append(result, intCommand)
  }

  for _, intCommand := range INT_SWITCH_COMMANDS {
    result = append(result, intCommand)
  }
  return result
}()

//ポケモンが重複しないようにアクセス制限
type Fighters struct {
  value [FIGHTERS_LENGTH]Pokemon
}

func (fighters *Fighters) PokeNameCount(pokeName *PokeName) int {
  result := 0
  for _, pokemon := range fighters.value {
    if pokeName.value == pokemon.Name.value {
      result += 1
    }
  }
  return result
}

func (fighters *Fighters) IsUnique() bool {
  for _, pokemon := range fighters.value {
    if fighters.PokeNameCount(&pokemon.Name) != 1 {
      return false
    }
  }
  return true
}

const NOT_UNIQUE_FIGHTERS_ERR_MSG = "ファイターズに同じ種類のポケモンを入れてはならない"

func NewFighters(value [FIGHTERS_LENGTH]Pokemon) (Fighters, error) {
  fighters := Fighters{value:value}
  if !fighters.IsUnique() {
    return Fighters{}, fmt.Errorf(NOT_UNIQUE_FIGHTERS_ERR_MSG)
  }
  return fighters, nil
}

func (fighters *Fighters) GetLegalMoveUseCommands() []MoveUseCommand {
  if fighters.IsFirstFaint() {
    return []MoveUseCommand{}
  }

  result := make([]MoveUseCommand, len(fighters.value[0].Moveset.value))

  for i := 0; i < len(result); i++ {
    result[i] = MoveUseCommand{value:INT_MOVE_USE_COMMANDS[i]}
  }
  return result
}

func (fighters *Fighters) IsLegalMoveUseCommand(moveUseCommand *MoveUseCommand) bool {
  legalMoveuseCommands := fighters.GetLegalMoveUseCommands()
  for _, command := range legalMoveuseCommands {
    if moveUseCommand.value == command.value {
      return true
    }
  }
  return false
}

func (fighters *Fighters) GetLegalSwitchCommands() []SwitchCommand {
  result := []SwitchCommand{}
  for i := 0; i < len(INT_SWITCH_COMMANDS); i++ {
    if !fighters.IsFaint(i + 1) {
      result = append(result, SwitchCommand{value:INT_SWITCH_COMMANDS[i]})
    }
  }
  return result
}

func (fighters *Fighters) GetRandomSwitchCommand(random *rand.Rand) SwitchCommand {
  legalSwitchCommands := fighters.GetLegalSwitchCommands()
  index := random.Intn(len(legalSwitchCommands))
  return legalSwitchCommands[index]
}

func (fighters *Fighters) IsLegalSwitchCommand(switchCommand *SwitchCommand) bool {
  legalSwitchCommands := fighters.GetLegalSwitchCommands()
  for _, command := range legalSwitchCommands {
    if command.value == switchCommand.value {
      return true
    }
  }
  return false
}

func (fighters *Fighters) GetLegalCommands() []Command {
  legalMoveuseCommands := fighters.GetLegalMoveUseCommands()
  legalSwitchCommands := fighters.GetLegalSwitchCommands()

  result := make([]Command, 0, len(legalMoveuseCommands) + len(legalSwitchCommands))

  for _, command := range legalMoveuseCommands {
    result = append(result, Command{value:command.value})
  }

  for _, command := range legalSwitchCommands {
    result = append(result, Command{value:command.value})
  }
  return result
}

func (fighters *Fighters) GetRandomLegalCommand(random *rand.Rand) Command {
  legalCommands := fighters.GetLegalCommands()
  index := random.Intn(len(legalCommands))
  return legalCommands[index]
}

func (fighters *Fighters) IsLegalCommands(command *Command) bool {
  legalCommands := fighters.GetLegalCommands()
  for _, tmpCommand := range legalCommands {
    if command.value == tmpCommand.value {
      return true
    }
  }
  return false
}

func NewMoveUseCommand(fighters *Fighters, value int) (MoveUseCommand, error) {
  result := MoveUseCommand{value:value}

  if !fighters.IsLegalMoveUseCommand(&result) {
    return MoveUseCommand{}, fmt.Errorf("合法ではない技コマンド")
  }

  if omw.IsIntInSliceInt(value, INT_MOVE_USE_COMMANDS) {
    return result, nil
  }

  errMsg := fmt.Sprintf("技コマンドは%vのいずれかでなければならない", INT_MOVE_USE_COMMANDS)
  return MoveUseCommand{}, fmt.Errorf(errMsg)
}

func NewSwitchCommand(fighters *Fighters, value int) (SwitchCommand, error) {
  result := SwitchCommand{value:value}

  if !fighters.IsLegalSwitchCommand(&result) {
    return SwitchCommand{}, fmt.Errorf("合法ではない交代コマンド")
  }

  if omw.IsIntInSliceInt(value, INT_SWITCH_COMMANDS) {
    return result, nil
  }

  errMsg := fmt.Sprintf("交代コマンドは%vのいずれかでなければならない", INT_SWITCH_COMMANDS)
  return SwitchCommand{}, fmt.Errorf(errMsg)
}

func NewCommand(fighters *Fighters, value int) (Command, error) {
  result := Command{value:value}

  if !fighters.IsLegalCommands(&result) {
    return Command{}, fmt.Errorf("合法ではないコマンド")
  }

  if omw.IsIntInSliceInt(value, INT_COMMANDS) {
    return result, nil
  }

  errMsg := fmt.Sprintf("コマンドは%vのいずれかでなければならない", INT_COMMANDS)
  return Command{}, fmt.Errorf(errMsg)
}

func (fighters *Fighters) IsFaint(index int) bool {
  return fighters.value[index].CurrentHP <= 0
}

func (fighters *Fighters) IsFirstFaint() bool {
  return fighters.IsFaint(0)
}

func (fighters *Fighters) IsAllFaint() bool {
  for i := 0; i < len(fighters.value); i++ {
    if !fighters.IsFaint(i) {
      return false
    }
  }
  return true
}

//将来的には、技の優先度も関係するので、fightersのメソッドとした
func (fighters *Fighters) GetActionPriorityRank(command *Command) int {
  _, err := NewMoveUseCommand(fighters, command.value)
  if err == nil {
    return 0
  }
  return 999
}

func (attackFighters *Fighters) GetSameTypeAttackBonus(moveName *MoveName) float64 {
  moveType := MOVEDEX[moveName.value].Type
  pokeName := attackFighters.value[0].Name
  pokeTypes := POKEDEX[pokeName.value].Types

  for _, pokeType := range pokeTypes {
    if moveType == pokeType {
      return 6144.0 / 4096.0
    }
  }
  return 6144.0 / 6144.0
}

func (defenseFighters *Fighters) GetEffectivenessBonus(moveName *MoveName) float64 {
  pokeName := defenseFighters.value[0].Name
  atkType := MOVEDEX[moveName.value].Type
  result := 1.0

  for _, defType := range POKEDEX[pokeName.value].Types {
    result *= TYPEDEX[atkType][defType]
  }
  return result
}

func (attackFighters *Fighters) GetDamageCalcAtkValue(moveName *MoveName) int {
  if MOVEDEX[moveName.value].Category == "物理" {
    return attackFighters.value[0].Atk
  }
  return attackFighters.value[0].SpAtk
}

func (defenseFighters *Fighters) GetDamageCalcDefValue(moveName *MoveName) int {
  if MOVEDEX[moveName.value].Category == "物理" {
    return defenseFighters.value[0].Def
  }
  return defenseFighters.value[0].SpDef
}

func (fighters Fighters) Switch(switchCommand *SwitchCommand) Fighters {
  if switchCommand.value == INT_SWITCH_COMMANDS[0] {
    value := [3]Pokemon{fighters.value[1], fighters.value[0], fighters.value[2]}
    return Fighters{value:value}
  }
  value := [3]Pokemon{fighters.value[2], fighters.value[1], fighters.value[0]}
  return Fighters{value:value}
}

func (fighters Fighters) IfFirstFaint_RandomSwitch(random *rand.Rand) Fighters {
  if fighters.IsFirstFaint() {
    switchCommand := fighters.GetRandomSwitchCommand(random)
    return fighters.Switch(&switchCommand)
  }
  return fighters
}

type Aspect struct {
  Self Fighters
  Opponent Fighters
}

func (aspect *Aspect) IsFirstFaintAny() bool {
  return aspect.Self.IsFirstFaint() || aspect.Opponent.IsFirstFaint()
}

//合法なコマンド以外入力出来ないようにアクセス制限
type BothCommand struct {
  self Command
  opponent Command
}

const NEW_BOTH_COMMAND_ERR_MSG = "味方/相手の先頭のどちらかが瀕死状態の場合、BothCommandは使用不可"

func NewBothCommand(aspect *Aspect, selfValue, opponentValue int) (BothCommand, error) {
  if aspect.IsFirstFaintAny() {
    return BothCommand{}, fmt.Errorf(NEW_BOTH_COMMAND_ERR_MSG)
  }

  selfCommand, err := NewCommand(&aspect.Self, selfValue)

  if err != nil {
    return BothCommand{}, err
  }

  opponentCommand, err := NewCommand(&aspect.Opponent, opponentValue)

  if err != nil {
    return BothCommand{}, err
  }

  return BothCommand{self:selfCommand, opponent:opponentCommand}, nil
}

type FightersMark struct {
  IsSelf bool
}

func (bothCommand *BothCommand) GetOneSideCommand(fightersMark *FightersMark) Command {
  if fightersMark.IsSelf {
    return bothCommand.self
  }
  return bothCommand.opponent
}

func (aspect *Aspect) GetReverseAspect() Aspect {
  result := Aspect{Self:aspect.Opponent, Opponent:aspect.Self}
  return result
}

type Winner struct {
  IsSelf bool
  IsOpponent bool
}

func (aspect *Aspect) GetActionPriorityWinner(bothCommand *BothCommand) Winner {
  selfPriority := aspect.Self.GetActionPriorityRank(&bothCommand.self)
  opponentPriority := aspect.Opponent.GetActionPriorityRank(&bothCommand.opponent)

  if selfPriority > opponentPriority {
    return Winner{IsSelf:true, IsOpponent:false}
  }

  if selfPriority < opponentPriority {
    return Winner{IsSelf:false, IsOpponent:true}
  }
  return Winner{IsSelf:false, IsOpponent:false}
}

func (aspect *Aspect) GetSpeedWinner() Winner {
  selfSpeed := aspect.Self.value[0].Speed
  opponentSpeed := aspect.Opponent.value[0].Speed

  if selfSpeed > opponentSpeed {
    return Winner{IsSelf:true, IsOpponent:false}
  }

  if selfSpeed < opponentSpeed {
    return Winner{IsSelf:false, IsOpponent:true}
  }
  return Winner{IsSelf:false, IsOpponent:false}
}

func (aspect *Aspect) IsFirstAction(bothCommand *BothCommand, random *rand.Rand) bool {
  priorityWinner := aspect.GetActionPriorityWinner(bothCommand)

  if priorityWinner.IsSelf {
    return true
  }

  if priorityWinner.IsOpponent {
    return false
  }

  speedWinner := aspect.GetSpeedWinner()

  if speedWinner.IsSelf {
    return true
  }

  if speedWinner.IsOpponent {
    return false
  }
  index := random.Intn(2)
  return []bool{true, false}[index]
}

const GET_ACTION_ORDER_PANIC_MSG = "GetActionOrderでライブラリのバグが発生した"

func (aspect *Aspect) GetActionOrder(bothCommand *BothCommand, random *rand.Rand) [2]FightersMark {
  selfFightersMark := FightersMark{IsSelf:true}
  opponentFIghtersMark := FightersMark{IsSelf:false}

  if aspect.IsFirstAction(bothCommand, random) {
    return [2]FightersMark{selfFightersMark, opponentFIghtersMark}
  }
  return [2]FightersMark{opponentFIghtersMark, selfFightersMark}
}

func (aspect *Aspect) GetDamege(moveName *MoveName) int {
  atkLevel := aspect.Self.value[0].Level
  power := MOVEDEX[moveName.value].Power
  calcAtk := aspect.Self.GetDamageCalcAtkValue(moveName)
  calcDef := aspect.Opponent.GetDamageCalcDefValue(moveName)

  result := (atkLevel.value * 2 / 5) + 2
  result = (result * power * calcAtk) / calcDef
  result = (result / 50) + 2
  result = omw.FiveOverInput(float64(result) * aspect.Self.GetSameTypeAttackBonus(moveName))
  result = int(float64(result) * aspect.Opponent.GetEffectivenessBonus(moveName))

  return result
}

func (aspect Aspect) MoveUse(moveUseCommand *MoveUseCommand) Aspect {
  moveName := aspect.Self.value[0].Moveset.value[moveUseCommand.value]
  damege := aspect.GetDamege(&moveName)
  if aspect.Opponent.value[0].CurrentHP - damege <= 0 {
    aspect.Opponent.value[0].CurrentHP = 0
    return aspect
  }
  aspect.Opponent.value[0].CurrentHP -= damege
  return aspect
}

const ACTION_PANIC_MSG = "Actionでライブラリのバグが発生した(Commandにユーザー側が不正入力した可能性もあり)"

func (aspect Aspect) Action(command *Command) Aspect {
  moveUseCommand, err := NewMoveUseCommand(&aspect.Self, command.value)

  if err == nil {
    return aspect.MoveUse(&moveUseCommand)
  }

  switchCommand, err := NewSwitchCommand(&aspect.Self, command.value)

  if err != nil {
    panic(ACTION_PANIC_MSG)
  }

  aspect.Self = aspect.Self.Switch(&switchCommand)
  return aspect
}

func (aspect Aspect) OpponentAction(command *Command) Aspect {
  aspect = aspect.GetReverseAspect()
  aspect = aspect.Action(command)
  result := aspect.GetReverseAspect()
  return result
}

func (aspect Aspect) FightersMarkToActionResult(command *Command, fightersMark *FightersMark) Aspect {
  if fightersMark.IsSelf {
    return aspect.Action(command)
  }
  return aspect.OpponentAction(command)
}

func (aspect *Aspect) IsGameEnd() bool {
  return aspect.Self.IsAllFaint() || aspect.Opponent.IsAllFaint()
}

const IS_GAME_WIN_ERR_MSG = "ゲームが終了していなければならない"

//現在は、勝ち or 負けの判定しかしないが、将来的には、引き分けの判定も必要なので、Winnerを戻り値にしておく
func (aspect *Aspect) GetGameWinner() (Winner, error) {
  if !aspect.IsGameEnd() {
    return Winner{}, fmt.Errorf(IS_GAME_WIN_ERR_MSG)
  }

  if aspect.Opponent.IsAllFaint() {
    return Winner{IsSelf:true, IsOpponent:false}, nil
  }
  return Winner{IsSelf:false, IsOpponent:true}, nil
}

const BOTH_ONE_TURN_RANDOM_PLAY_PANIC_MSG = "BothOneTurnRandomPlayでライブラリのバグが発生した"

//現状は、以下のリンクに書かれてある「1～2」の処理を1ターンとする
//http://pokemon-wiki.net/%E3%83%90%E3%83%88%E3%83%AB%E4%B8%AD%E3%81%AE%E5%87%A6%E7%90%86%E3%81%AE%E9%A0%86%E7%95%AA#battle_flowchart
func (aspect Aspect) BothOneTurnRandomPlay(random *rand.Rand) Aspect {
  aspect.Self = aspect.Self.IfFirstFaint_RandomSwitch(random)
  aspect.Opponent = aspect.Opponent.IfFirstFaint_RandomSwitch(random)

  selfCommand := aspect.Self.GetRandomLegalCommand(random)
  opponentCommand := aspect.Opponent.GetRandomLegalCommand(random)

  bothCommand, err := NewBothCommand(&aspect, selfCommand.value, opponentCommand.value)

  if err != nil {
    panic(BOTH_ONE_TURN_RANDOM_PLAY_PANIC_MSG)
  }

  actionOrder := aspect.GetActionOrder(&bothCommand, random)

  for _, fightersMark := range actionOrder {
    command := bothCommand.GetOneSideCommand(&fightersMark)
    aspect = aspect.FightersMarkToActionResult(&command, &fightersMark)
    if aspect.IsFirstFaintAny() {
      break
    }
  }
  return aspect
}

const ONE_PLAY_OUT_PANIC_MSG = "OnePlayoutでライブラリのバグが発生した"

func (aspect Aspect) OnePlayout(random *rand.Rand) int {
  for {
    aspect = aspect.BothOneTurnRandomPlay(random)

    if aspect.IsGameEnd() {
      break
    }
  }

  isGameWin, err := aspect.GetGameWinner()

  if err != nil {
    panic(ONE_PLAY_OUT_PANIC_MSG)
  }

  if isGameWin.IsSelf {
    return 1
  }
  return 0
}

func (aspect Aspect) Playout(random *rand.Rand, simuNum int) float64 {
  result := 0.0
  for i := 0; i < simuNum; i++ {
    result += float64(aspect.OnePlayout(random))
  }
  return result / float64(simuNum)
}
