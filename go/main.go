package main

import (
  "fmt"
  "net"
  "strings"
  "strconv"
  "omw"
  "bippa"
)

type TeamInfo struct {
  pokeNames [6]string
  levels [6]int
  natureNames [6]string

  move1Names [6]string
  move2Names [6]string
  move3Names [6]string
  move4Names [6]string

  hpIVS [6]int
  atkIVS [6]int
  defIVS [6]int
  spAtkIVS [6]int
  spDefIVS [6]int
  speedIVS [6]int

  hpEVS [6]int
  atkEVS [6]int
  defEVS [6]int
  spAtkEVS [6]int
  spDefEVS [6]int
  speedEVS [6]int
}

func splitToTeamInfoStrField(split []string) [6]string {
  var result [6]string
  for i, ele := range split[1:] {
    result[i] = ele
  }
  return result
}

func splitToTeamInfoIntField(split []string) [6]int {
  var result [6]int
  var value int
  var err error

  for i, ele := range split[1:] {
    if ele == "None" {
      value = -1
    } else {
      value, err = strconv.Atoi(ele)
      if err != nil {
        panic(err)
      }
    }
    result[i] = value
  }
  return result
}

func recvTeamInfo(conn net.Conn, color string) TeamInfo {
  result := TeamInfo{}

  for i := 0; i < 19; i++ {
    omw.SendMsg(conn, color)
    msg := omw.RecvMsg(conn, 1024)
    split := strings.Fields(msg)

    if split[0] == "poke_names" {
      result.pokeNames = splitToTeamInfoStrField(split)
    }

    if split[0] == "levels" {
      result.levels = splitToTeamInfoIntField(split)
    }

    if split[0] == "nature_names" {
      result.natureNames = splitToTeamInfoStrField(split)
    }

    if split[0] == "move1_names" {
      result.move1Names = splitToTeamInfoStrField(split)
    }

    if split[0] == "move2_names" {
      result.move2Names = splitToTeamInfoStrField(split)
    }

    if split[0] == "move3_names" {
      result.move3Names = splitToTeamInfoStrField(split)
    }

    if split[0] == "move4_names" {
      result.move4Names = splitToTeamInfoStrField(split)
    }

    if split[0] == "hp_ivs" {
      result.hpIVS = splitToTeamInfoIntField(split)
    }

    if split[0] == "atk_ivs" {
      result.atkIVS = splitToTeamInfoIntField(split)
    }

    if split[0] == "def_ivs" {
      result.defIVS = splitToTeamInfoIntField(split)
    }

    if split[0] == "sp_atk_ivs" {
      result.spAtkIVS = splitToTeamInfoIntField(split)
    }

    if split[0] == "sp_def_ivs" {
      result.spDefIVS = splitToTeamInfoIntField(split)
    }

    if split[0] == "speed_ivs" {
      result.speedIVS = splitToTeamInfoIntField(split)
    }

    if split[0] == "hp_evs" {
      result.hpEVS = splitToTeamInfoIntField(split)
    }

    if split[0] == "atk_evs" {
      result.atkEVS = splitToTeamInfoIntField(split)
    }

    if split[0] == "def_evs" {
      result.defEVS = splitToTeamInfoIntField(split)
    }

    if split[0] == "sp_atk_evs" {
      result.spAtkEVS = splitToTeamInfoIntField(split)
    }

    if split[0] == "sp_def_evs" {
      result.spDefEVS = splitToTeamInfoIntField(split)
    }

    if split[0] == "speed_evs" {
      result.speedEVS = splitToTeamInfoIntField(split)
    }
  }
  return result
}

func teamInfoMoveNamesToMoveset(pokeName *bippa.PokeName, move1Names, move2Names,
                                move3Names, move4Names [6]string, index int) bippa.Moveset {
  strMoveset := make([]string, 0, 1)
  for _, strMoveName := range []string{move1Names[index], move2Names[index], move3Names[index], move4Names[index]} {
    if strMoveName == "なし" || strMoveName == "None" {
      continue
    }
    strMoveset = append(strMoveset, strMoveName)
  }

  result, err := bippa.NewMoveset(pokeName, strMoveset)
  if err != nil {
    panic(err)
  }
  return result
}

func teamInfoIVToIVStats(hpIVS, atkIVS, defIVS, spAtkIVS, spDefIVS, speedIVS [6]int, index int) bippa.IVStats {
  result, err := bippa.NewIVStats(hpIVS[index], atkIVS[index], defIVS[index],
                                  spAtkIVS[index], spDefIVS[index], speedIVS[index])

  if err != nil {
    panic(err)
  }
  return result
}

func teamInfoEVToEVStats(hpEVS, atkEVS, defEVS, spAtkEVS, spDefEVS, speedEVS [6]int, index int) bippa.EVStats {
  result, err := bippa.NewEVStats(hpEVS[index], atkEVS[index], defEVS[index],
                                  spAtkEVS[index], spDefEVS[index], speedEVS[index])

  if err != nil {
    panic(err)
  }
  return result
}

func teamInfoToTeam(teamInfo TeamInfo) bippa.Team {
  teamValue := make([]bippa.Pokemon, 0, 3)

  for i := 0; i < 6; i++ {
    strPokeName := teamInfo.pokeNames[i]

    if strPokeName == "なし" {
      continue
    }

    pokeName, err := bippa.NewPokeName(strPokeName)

    if err != nil {
      panic(err)
    }

    level, err := bippa.NewLevel(teamInfo.levels[i])

    if err != nil {
      panic(err)
    }

    natureName, err := bippa.NewNatureName(teamInfo.natureNames[i])

    if err != nil {
      panic(err)
    }

    moveset := teamInfoMoveNamesToMoveset(&pokeName, teamInfo.move1Names, teamInfo.move2Names,
                                          teamInfo.move3Names, teamInfo.move4Names, i)

    if err != nil {
      panic(err)
    }

    ivStats := teamInfoIVToIVStats(teamInfo.hpIVS, teamInfo.atkIVS, teamInfo.defIVS,
                                   teamInfo.spAtkIVS, teamInfo.spDefIVS, teamInfo.speedIVS, i)

    evStats := teamInfoEVToEVStats(teamInfo.hpEVS, teamInfo.atkEVS, teamInfo.defEVS,
                                   teamInfo.spAtkEVS, teamInfo.spDefEVS, teamInfo.speedEVS, i)

    pokemon := bippa.NewPokemon(&pokeName, &level, &natureName, &moveset, &ivStats, &evStats)
    teamValue = append(teamValue, pokemon)
  }

  result, err := bippa.NewTeam(teamValue)

  if err != nil {
    panic(err)
  }
  return result
}

func main() {
  fmt.Println("サーバーへ接続中...")
  conn, err := net.Dial("tcp", "localhost:7777")
  if err != nil {
    panic(err)
  }
  fmt.Println("サーバーに接続完了")

  redTeamInfo := recvTeamInfo(conn, "red")
  blueTeamInfo := recvTeamInfo(conn, "blue")
  fmt.Println(redTeamInfo)
  fmt.Println(blueTeamInfo)

  redTeam := teamInfoToTeam(redTeamInfo)
  blueTeam := teamInfoToTeam(blueTeamInfo)

  fmt.Println(redTeam)
  fmt.Println(blueTeam)
}
