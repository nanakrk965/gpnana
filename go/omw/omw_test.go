package omw

import (
  "testing"
)

const TEST_DATA_PATH = "test_data/"

func TestGetFileNames(t *testing.T) {
  result, err := GetFileNames(TEST_DATA_PATH)

  if err != nil {
    panic(err)
  }

  expected := []string{"bar.csv", "foo.txt"}

  if len(result) != len(expected) {
    t.Errorf("テスト失敗")
  }

  for i, fileName := range expected {
    if result[i] != fileName {
      t.Errorf("テスト失敗")
    }
  }
}

func TestReadTxtLinesSJS(t *testing.T) {
  result, err := ReadTxtLinesSJS(TEST_DATA_PATH + "foo.txt", 2024)

  if err != nil {
    panic(err)
  }

  expected := []string{"ぴよ ぽこ", "C++ Java", "Python Go"}

  if len(result) != len(expected) {
    t.Errorf("テスト失敗")
  }

  for i, line := range expected {
    if result[i] != line {
      t.Errorf("テスト失敗")
    }
  }
}

func TestReadCsvLinesSJS(t *testing.T) {
  result, err := ReadCsvLinesSJS(TEST_DATA_PATH + "bar.csv")

  if err != nil {
    panic(err)
  }

  expected := [][]string{{"ぴよ", "ぽこ"}, {"C++", "Java"}, {"Python", "Go"}}

  if len(result) != len(expected) {
    t.Errorf("テスト失敗")
  }

  for i, line := range expected {
    if !IsEqualSliceStr(line, expected[i]) {
      t.Errorf("テスト失敗")
    }
  }
}

func TestAll(t *testing.T) {
  if !All([]bool{true, true, true}) {
    t.Errorf("テスト失敗")
  }

  if All([]bool{true, false, true}) {
    t.Errorf("テスト失敗")
  }
}

func TestAny(t *testing.T) {
  if !Any([]bool{true, false, false}) {
    t.Errorf("テスト失敗")
  }

  if Any([]bool{false, false, false}) {
    t.Errorf("テスト失敗")
  }
}

func TestCount(t *testing.T) {
  count := Count{0}
  if count.GetIncrement() != 1 {
    t.Errorf("テスト失敗")
  }

  if count.GetIncrement() != 2 {
    t.Errorf("テスト失敗")
  }

  if count.GetDecrement() != 1 {
    t.Errorf("テスト失敗")
  }

  if count.GetDecrement() != 0 {
    t.Errorf("テスト失敗")
  }
}

func TestIsEqualSliceStr(t *testing.T) {
  data1 := []string{"Go", "Java", "Python"}
  data2 := []string{"Go", "Java", "Python"}

  if !IsEqualSliceStr(data1, data2) {
    t.Errorf("テスト失敗")
  }

  data1 = []string{"C++", "Java", "Python", "Go"}
  data2 = []string{"C++", "Java", "Go", "Python"}

  if IsEqualSliceStr(data1, data2) {
    t.Errorf("テスト失敗")
  }

  data1 = []string{"C++", "Java", "Python", "Go"}
  data2 = []string{"C++", "Java", "Python"}

  if IsEqualSliceStr(data1, data2) {
    t.Errorf("テスト失敗")
  }
}
