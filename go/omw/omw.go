package omw

import (
  "net"
  "os"
  "io"
  "io/ioutil"
  "bufio"
  "strings"
  "encoding/csv"
  "golang.org/x/text/encoding/japanese"
  "golang.org/x/text/transform"
)

func GetFileNames(folderPath string) ([]string, error) {
  result := []string{}
  files, err := ioutil.ReadDir(folderPath)

  if err != nil {
    return []string{}, err
  }

  for _, file := range files {
    result = append(result, file.Name())
  }
  return result, nil
}

func DecodeShiftJIS(str string) (string, error) {
  ret, err := ioutil.ReadAll(transform.NewReader(strings.NewReader(str),
                             japanese.ShiftJIS.NewDecoder()))
  if err != nil {
    return "", err
  }
  return string(ret), err
}

func ReadTxtLinesSJS(filePath string, size int) ([]string, error) {
  result := []string{}
  file, err := os.Open(filePath)

  if err != nil {
    return nil, err
  }

  reader := bufio.NewReaderSize(file, size)
  var line []byte
  var decodeSJS string

  for {
    line, _, err = reader.ReadLine()

    if err == io.EOF {
      break
    } else if err != nil {
      return []string{}, err
    }

    decodeSJS, err = DecodeShiftJIS(string(line))
    if err != nil {
      panic(err)
    }
    result = append(result, decodeSJS)
  }
  return result, nil
}

func ReadCsvLinesSJS(filePath string) ([][]string, error) {
  result := [][]string{}
  file, err := os.Open(filePath)
  if err != nil {
    return nil, err
  }
  defer file.Close()

  reader := csv.NewReader(file)
  var line []string
  var decodeSJSLine []string

  for {
    line, err = reader.Read()
    if err != nil {
      break
    }
    decodeSJSLine = make([]string, len(line))
    for i, ele := range line {
      decodeSJSLine[i], err = DecodeShiftJIS(ele)
      if err != nil {
        panic(err)
      }
    }
    result = append(result, decodeSJSLine)
  }
  return result, nil
}

func All(data []bool) bool {
  for _, ele := range data {
    if !ele {
      return false
    }
  }
  return true
}

func Any(data []bool) bool {
  for _, ele := range data {
    if ele {
      return true
    }
  }
  return false
}

func SendMsg(conn net.Conn, msg string) {
  conn.Write([]byte(msg))
}

func RecvMsg(conn net.Conn, size int) string {
  buf := make([]byte, size)
  bufLen, _ := conn.Read(buf)
  return string(buf[:bufLen])
}

type Count struct {
  Value int
}

func (count *Count) GetIncrement() int {
  count.Value += 1
  return count.Value
}

func (count *Count) GetDecrement() int {
  count.Value -= 1
  return count.Value
}

func GetSliceIntCount(sliceInt []int, x int) int {
  result := 0
  for _, ele := range sliceInt {
    if x == ele {
      result += 1
    }
  }
  return result
}

func IsUniqueSliceInt(sliceInt []int) bool {
  for _, ele := range sliceInt {
    if GetSliceIntCount(sliceInt, ele) != 1 {
      return false
    }
  }
  return true
}

func IsIntInSliceInt(x int, sliceInt []int) bool {
  for _, ele := range sliceInt {
    if x == ele {
      return true
    }
  }
  return false
}

func IsEqualSliceStr(data1, data2 []string) bool {
  data1Length := len(data1)

  if data1Length != len(data2) {
    return false
  }

  for i := 0; i < data1Length; i++ {
    if data1[i] != data2[i] {
      return false
    }
  }
  return true
}

//五捨五超入
func FiveOverInput(x float64) int {
  if int(x + 0.49999) == int(x) {
    return int(x)
  } else {
    return int(x + 1)
  }
}
